from django.contrib.auth import get_user_model

from rest_framework import serializers

from onlytexts.models import Post, Background, Comment, PostStatus, CommentStatus


class UserSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = get_user_model()
		fields = ('id', 'username', 'email', 'first_name', 'last_name', 'country', 'avatar')


class PostSerializer(serializers.ModelSerializer):
	votes = serializers.IntegerField(source='get_vote_count', read_only=True)
	comments = serializers.IntegerField(source='get_comment_count', read_only=True)

	class Meta:
		model = Post
		fields = ('uploader', 'title', 'content', 'background', 'category', 'votes', 'comments', 'timestamp')
		read_only_fields = ('uploader', 'timestamp', 'votes', 'comments')


class BackgroundSerializer(serializers.ModelSerializer):
	image_url = serializers.URLField(source='image.url')

	class Meta:
		model = Background
		fields = ('title', 'image_url', 'id')


class CommentUserSerializer(serializers.ModelSerializer):

	class Meta:
		model = get_user_model()
		fields = ('id', 'username')


class CommentSerializer(serializers.ModelSerializer):
	commentor = CommentUserSerializer(read_only=True)
	votes = serializers.IntegerField(source='get_vote_count')
	comments = serializers.IntegerField(source='get_comment_count')

	class Meta:
		model = Comment
		fields = ('id', 'commentor', 'text', 'votes', 'comments', 'timestamp')
		read_only_fields = ('timestamp', 'votes', 'comments')


class PostStatusSerializer(serializers.ModelSerializer):

	class Meta:
		model = PostStatus
		exclude = ('id', 'post')
		read_only_fields = ('user', )


class CommentStatusSerializer(serializers.ModelSerializer):

	class Meta:
		model = CommentStatus
		exclude = ('id', 'comment')
		read_only_fields = ('user', )