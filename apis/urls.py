from django.conf.urls import url, include
# from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from .views import UserList, UserDetail, PostList, BackgroundList, BackgroundDetail, CommentDetail, PostStatusDetail, CommentStatusDetail

app_name = "apis"
# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)

urlpatterns = [
	url(r'^users/?$', UserList.as_view()),
	url(r'^users/(?P<pk>\d+)/?$', UserDetail.as_view()),
	url(r'^posts/?$', PostList.as_view()),
	url(r'^posts/(?P<post_id>\d+)/vote/?$', PostStatusDetail.as_view()),
	url(r'^posts/(?P<post_id>\d+)/comments/?$', CommentDetail.as_view()),
	url(r'^posts/(?P<post_id>\d+)/comments/(?P<comm_id>\d+)/?$', CommentDetail.as_view()),
	url(r'^comments/(?P<comm_id>\d+)/vote/?$', CommentStatusDetail.as_view()),
	url(r'^backgrounds/?$', BackgroundList.as_view()),
	url(r'^backgrounds/(?P<pk>\d+)/?$', BackgroundDetail.as_view()),
	url(r'^api-auth/', include('rest_framework.urls'), name='rest_framework')
]

urlpatterns = format_suffix_patterns(urlpatterns)