from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db.models import F

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status, serializers

from onlytexts.models import Post, Background, Comment, PostStatus, CommentStatus

from .serializers import UserSerializer, PostSerializer, BackgroundSerializer, CommentSerializer, PostStatusSerializer, CommentStatusSerializer
from .permissions import IsOwnerOnly


class UserList(APIView):
	def get(self, request, format=None):
		users = UserSerializer(get_user_model().objects.all(), many=True)
		return Response(users.data)


class UserDetail(APIView):
	permission_classes = (permissions.IsAuthenticated, IsOwnerOnly)

	def get(self, request, pk, format=None):
		obj = get_object_or_404(get_user_model(), pk=pk)
		user = UserSerializer(obj)
		self.check_object_permissions(request, pk)
		return Response(user.data)


class PostList(APIView):

	def get(self, request, format=None):
		posts = PostSerializer(Post.objects.all(), many=True)
		return Response(posts.data)

	def post(self, request, format=None):
		serializer = PostSerializer(data=request.data)
		user = request.user

		if serializer.is_valid():
			serializer.save(uploader=user)
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BackgroundList(APIView):

	def get(self, request, format=None):
		backgrounds = BackgroundSerializer(Background.objects.all(), many=True)
		return Response(backgrounds.data)


class BackgroundDetail(APIView):

	def get(self, request, pk, format=None):
		obj = get_object_or_404(Background, pk=pk)
		background = BackgroundSerializer(obj)
		return Response(background.data)


class CommentDetail(APIView):

	def get(self, request, post_id, comm_id=None, format=None):
		post = get_object_or_404(Post, pk=post_id)
		if comm_id:
			comment = get_object_or_404(Comment, pk=comm_id)
		else:
			comment = None
		obj = Comment.objects.filter(post=post, parent=comment)
		comments = CommentSerializer(obj,  many=True)
		return Response(comments.data)

	def post(self, request, post_id, comm_id=None, format=None):
		serializer = CommentSerializer(data=request.data)
		post = get_object_or_404(Post, pk=post_id)
		user = request.user
		parent = None
		if not user.is_authenticated:
			raise serializers.ValidationError("User Not Authenticated")
		if comm_id:
			parent = get_object_or_404(Comment, pk=comm_id)
			if str(parent.post.id) != post_id:
				raise serializers.ValidationError("Invalid Request")
		if serializer.is_valid():
			serializer.save(commentor=user, post=post, parent=parent)
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PostStatusDetail(APIView):

	def get(self, request, post_id):
		post = get_object_or_404(Post, pk=post_id)
		post_status = PostStatus.objects.filter(post=post)
		serializer = PostStatusSerializer(post_status, many=True)
		return Response(serializer.data)

	def post(self, request, post_id, format=None):
		post = get_object_or_404(Post, pk=post_id)
		user = request.user
		if not user.is_authenticated:
			raise serializers.ValidationError("User not Authenticated")
		post_status = PostStatus.objects.filter(user=user, post=post)
		serializer = PostStatusSerializer(data=request.data)
		if post_status:
			serializer = PostStatusSerializer(post_status[0], data=request.data)
		if serializer.is_valid():
			serializer.save(user=user, post=post)
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CommentStatusDetail(APIView):

	def get(self, request, comm_id):
		comment = get_object_or_404(Comment, pk=comm_id)
		comment_status = CommentStatus.objects.filter(comment=comment)
		serializer = CommentStatusSerializer(comment_status, many=True)
		return Response(serializer.data)

	def post(self, request, comm_id, format=None):
		comment = get_object_or_404(Comment, pk=comm_id)
		user = request.user
		if not user.is_authenticated:
			raise serializers.ValidationError("User not Authenticated")
		comment_status = CommentStatus.objects.filter(user=user, comment=comment)
		serializer = CommentStatusSerializer(data=request.data)
		if comment_status:
			serializer = PostStatusSerializer(comment_status[0], data=request.data)
		if serializer.is_valid():
			serializer.save(user=user, comment=comment)
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
