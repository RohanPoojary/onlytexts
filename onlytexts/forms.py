from django import forms
from django.utils.translation import ugettext_lazy as _
from . import models


class UserRegisterForm(forms.ModelForm):
	verify = forms.CharField(label=_('Reenter Password'), max_length=40, widget=forms.TextInput(attrs={'type': 'password', 'placeholder': 'Re Enter Password'}))

	class Meta:
		model = models.User
		fields = ['username', 'email', 'password', 'verify', 'first_name', 'last_name', 'country', 'avatar']
		widgets = {
			'password': forms.PasswordInput()
		}
		labels = {
			'username': _('Username'),
			'email': _('Email Id'),
			'password': _('Password'),
			'first_name': _('First Name'),
			'last_name': _('Last Name'),
			'country': _('Country'),
			'avatar': _('Profile Image')
		}

	def clean(self):
		cleaned_data = super(UserRegisterForm, self).clean()
		verify = cleaned_data.get("verify")
		password = cleaned_data.get("password")
		if verify != password:
			raise forms.ValidationError(
				_('Password Mismatch'),
				code="Invalid"
			)


class UserLoginForm(forms.Form):
	username = forms.CharField(label=_("Email Id / Username"), max_length=40)
	password = forms.CharField(label=_("Password"), max_length=50, widget=forms.PasswordInput())


class PostUploadForm(forms.ModelForm):

	class Meta:
		model = models.Post
		fields = ['title', 'content', 'category', 'background']
		labels = {
			'title': _('Title'),
			'content': _('Content'),
			'category': _('Category'),
			'background': _('Background')
		}
