from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import reverse, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .forms import UserRegisterForm, UserLoginForm, PostUploadForm


def login_view(request):
	redirect_uri = request.GET.get('next', reverse('onlytexts:login_success'))
	action_uri = request.path + "?next=%s" % redirect_uri
	if request.method == "POST":
		form = UserLoginForm(request.POST)
		if form.is_valid():
			user = authenticate(email=form.cleaned_data["username"], password=form.cleaned_data["password"])
			if user is not None:
				login(request, user=user)
				return HttpResponseRedirect(redirect_uri)
			else:
				form.add_error(None, "Incorrect Email or Password")
		context = {
			'form': form,
			'action_uri': action_uri
		}
		return render(request, 'onlytexts/login.html', context=context)
	else:
		if request.user.is_authenticated:
			return HttpResponseRedirect(redirect_uri)
		context = {
			'form': UserLoginForm(),
			'action_uri': action_uri
		}
		return render(request, 'onlytexts/login.html', context=context)


def logout_view(request):
	logout(request)
	return HttpResponse("Logged Out")


def signup(request):
	if request.method == "POST":
		form = UserRegisterForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse("onlytexts:signup_success"))
		else:
			context = {
				"form": form
			}
			return render(request, 'onlytexts/signup.html', context=context)
	else:
		context = {
			"form": UserRegisterForm()
		}
		return render(request, 'onlytexts/signup.html', context=context)


@login_required()
def upload(request):
	if request.method == "POST":
		form = PostUploadForm(request.POST, request.FILES)
		if form.is_valid():
			post = form.save(commit=False)
			post.uploader = request.user
			post.save()
			return HttpResponseRedirect(reverse("onlytexts:signup_success"))
		else:
			context = {
				"form": form
			}
			return render(request, 'onlytexts/upload.html', context=context)
	else:
		context = {
			"form": PostUploadForm()
		}
		return render(request, 'onlytexts/upload.html', context=context)


def signup_s(request):
	return HttpResponse("Success")


@login_required()
def login_s(request):
	return HttpResponse("Success")


def index(request):
	return render(request, 'onlytexts/index.html')
