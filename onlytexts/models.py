from __future__ import unicode_literals

from django.db import models
from django.core.validators import validate_slug
from django.utils.translation import ugettext as _
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.conf import settings
from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
	username = models.CharField(_('Username'), max_length=30, unique=True, blank=False, validators=[validate_slug])
	email = models.EmailField(_('Email Id'), unique=True, blank=False)
	first_name = models.CharField(_('First Name'), max_length=30)
	last_name = models.CharField(_('Last Name'), max_length=30)
	date_joined = models.DateTimeField(_('Date Joined'), auto_now_add=True)
	is_active = models.BooleanField(_('is Active'), default=True)
	is_staff = models.BooleanField(_('is Staff'), default=False)
	avatar = models.ImageField(_('Profile Image'), upload_to='avatars/', null=True, blank=True)
	COUNTRY_CHOICES = (
		('IND', _('India')),
		('NA', _('North America'))
	)
	country = models.CharField(_('Country'), choices=COUNTRY_CHOICES, max_length=4)

	objects = UserManager()

	USERNAME_FIELD = "email"
	REQUIRED_FIELDS = ["username"]

	class Meta:
		verbose_name = _('user')
		verbose_name_plural = _('users')

	def get_full_name(self):
		return self.first_name + " " + self.last_name

	def get_short_name(self):
		return self.username


class Background(models.Model):
	image = models.ImageField(_('Background Image'), upload_to='backgrounds/')
	title = models.CharField(_('Background Title'), max_length=20)

	def __str__(self):
		return self.title


class Category(models.Model):
	name = models.CharField(_('Name'), max_length=15)
	code = models.CharField(_('Code'), max_length=3, primary_key=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = _('category')
		verbose_name_plural = _('categories')


class Post(models.Model):
	title = models.CharField(_('Title'), max_length=50)
	uploader = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='posts', on_delete=models.CASCADE)
	content = models.TextField(_('Content'))
	timestamp = models.DateTimeField(_('Uploaded at'), auto_now_add=True)
	background = models.ForeignKey(Background, blank=True, null=True, on_delete=models.SET_NULL)
	category = models.ManyToManyField(Category)

	def __str__(self):
		return self.title

	def get_vote_count(self):
		count = 0
		for vote in self.post_status.all():
			count += vote.status
		return count

	def get_comment_count(self):
		return self.comments.count()


class Comment(models.Model):
	commentor = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='comments', on_delete=models.CASCADE)
	text = models.CharField(_('Text'), max_length=500)
	timestamp = models.DateTimeField(_('Commented at'), auto_now_add=True)
	post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
	parent = models.ForeignKey('self', related_name='comments', blank=True, null=True, on_delete=models.CASCADE)

	def __str__(self):
		return self.text

	def get_vote_count(self):
		count = 0
		for vote in self.comment_status.all():
			count += vote.status
		return count

	def get_comment_count(self):
		return self.comments.count()


class PostStatus(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='post_status', on_delete=models.CASCADE)
	post = models.ForeignKey(Post, related_name='post_status', on_delete=models.CASCADE)
	CHOICES = (
		(1, 1),
		(0, 0),
		(-1, -1)
	)
	status = models.IntegerField(_('Status'), choices=CHOICES)

	class Meta:
		unique_together = (('user', 'post'),)

	def __str__(self):
		return str(self.status)


class CommentStatus(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='comment_status', on_delete=models.CASCADE)
	comment = models.ForeignKey(Comment, related_name='comment_status', on_delete=models.CASCADE)
	CHOICES = (
		(1, 1),
		(0, 0),
		(-1, -1)
	)
	status = models.IntegerField(_('Status'), choices=CHOICES)

	def __str__(self):
		return self.status
