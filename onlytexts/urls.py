from django.conf.urls import url

from .views import signup, signup_s, login_view, login_s, logout_view, upload, index

app_name = "onlytexts"

urlpatterns = [
	url(r'^/?$', index, name="index"),
	url(r'^signup/$', signup, name="signup"),
	url(r'^signup-success/$', signup_s, name="signup_success"),
	url(r'^login/$', login_view, name="login"),
	url(r'^login-success/$', login_s, name="login_success"),
	url(r'^logout/$', logout_view, name="logout"),
	url(r'^upload/$', upload, name="upload"),
]
