from django.contrib import admin
from .models import User, Background, Post, Category, Comment

# Register your models here.
admin.site.register(User)
admin.site.register(Background)
admin.site.register(Category)
admin.site.register(Post)
admin.site.register(Comment)
